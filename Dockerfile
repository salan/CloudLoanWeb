FROM python:3.10-slim

COPY . ./cloudloanweb

RUN python -m pip install --upgrade pip
#    &&
RUN pip install -r ./cloudloanweb/requirements.txt -i https://mirrors.aliyun.com/pypi/simple/

#COPY requirements.txt .
#
#RUN pip install -r requirements.txt

ENV env=prod
ENV system=linux
ENV casepath=./cloudloanweb/cloudloanweb_cases

CMD python ./cloudloanweb/run.py $env $system "./cloudloanweb/$casepath"