# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: 登录神卫
"""

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

from common.base import Base, Common


class Login(Base):

	def login(self, env: str):
		"""
		:param env: saas_qa/saas_test
		:return: no return
		"""
		account_number = (By.CSS_SELECTOR, "input[placeholder='请输入账号']")
		password = (By.CSS_SELECTOR, "input[type='password']")
		sms_code = (By.CSS_SELECTOR, "input[placeholder='请输入短信验证码']")
		login_button = (By.CSS_SELECTOR, "[type=button]:nth-child(1)")
		change_pwd = (By.XPATH, "//h4[text()='密码重置']")
		new_pwd = (By.XPATH, "(//input[@placeholder='请输入新密码'])[1]")
		confirm_pwd = (By.XPATH, "(//input[@placeholder='请输入新密码'])[2]")
		confirm_button = (By.XPATH, "(//button)[1]")
		login_now = (By.XPATH, "(//button)[2]")
		user_data = Common.get_yaml_data("config", "user_data.yaml")

		self.send_keys(*account_number, text=user_data[env]["username"])
		self.send_keys(*password, text=user_data[env]["password"])
		self.send_keys(*sms_code, text="1")
		self.element_click(*login_button)
		c = EC.invisibility_of_element_located(change_pwd)
		if not c(self.driver):
			print("已进入密码修改页面,即将进行密码修改!")
			if env == "saas_qa":
				self.send_keys(*new_pwd, text="Admin123.")
				self.send_keys(*confirm_pwd, text="Admin123.")
			elif env == "saas_test":
				self.send_keys(*new_pwd, text="Asd123456.")
				self.send_keys(*confirm_pwd, text="Asd123456.")
			else:
				raise KeyError("未知的账号")
			self.element_click(*confirm_button)
			self.element_click(*login_now)
			self.send_keys(*account_number, text=user_data[env]["username"])
			self.send_keys(*password, text=user_data[env]["password"])
			self.send_keys(*sms_code, text="1")
			self.element_click(*login_button)
			self.open()

	def login_plms(self, env: str):
		"""
		:param env: plms_qa/plms_prod
		:return: no return
		"""
		user_name = (By.NAME, "username")
		password = (By.NAME, "password")
		login_button = (By.CSS_SELECTOR, ".el-button--medium")
		user_data = Common.get_yaml_data("config", "user_data.yaml")

		self.send_keys(*user_name, text=user_data[env]["username"])
		self.send_keys(*password, text=user_data[env]["password"])
		self.element_click(*login_button)
