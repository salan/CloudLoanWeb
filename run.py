# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: 
"""
import sys
from multiprocessing import Pool, Manager

import pytest

from common.base import Common


# noinspection PySimplifyBooleanCheck
def get_browser(platform: list):
	plat = []
	env = Common.get_yaml_data("config", "env.yaml")
	for i in platform:
		if env[i]["browser"]:
			for b in env[i]["browser"]:
				browser = [i, b]
				plat.append(browser)
	print("运行环境:", plat)
	return plat


def run_case(case: list):
	res = pytest.main(
		[f"{sys.argv[3]}", f"--env={sys.argv[1]}", "--reruns=1", f"--platform={case[0]}",
		 f"--browser={case[1]}"])
	# RESULT_CODE.append(res)
	return res


def run_pool(cases: list):
	pool = Pool(len(cases))
	# for case in cases:
	# 	pool.apply_async(run_case, (case,))
	res = pool.map(run_case, cases)
	return res


# pool.close()
# pool.join()

def main():
	res = run_pool(get_browser(sys.argv[2].split(",")))
	print(f"各进程返回Code:{res}")
	if len(res) == 0:
		sys.exit(1)
	for r in res:
		if r != 0:
			sys.exit(r)


if __name__ == '__main__':
	# manager = Manager()
	# RESULT_CODE = manager.list()
	# run_pool(get_browser(sys.argv[2].split(",")))
	# print(f"各进程返回Code:{RESULT_CODE}")
	# if len(RESULT_CODE) == 0:
	# 	sys.exit(1)
	# for r in RESULT_CODE:
	# 	if r != 0:
	# 		sys.exit(RESULT_CODE)
	# manager = Manager()
	# RESULT_CODE = manager.list()
	main()
