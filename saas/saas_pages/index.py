# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: saas主页
"""
import time
import allure

from selenium.webdriver.common.by import By
from assertpy.assertpy import assert_that, soft_assertions
from saas.saas_pages.other_pages import *
from common.base import Base


class Index(Base):
	amount = (By.XPATH, '//p[contains(text(),"在贷总金额")]/preceding-sibling::*[1]')
	running_days = (By.XPATH, '//p[contains(text(),"业务运行天数")]/preceding-sibling::*[1]')
	user_count = (By.XPATH, '//p[contains(text(),"用户数")]/preceding-sibling::*[1]')
	count = (By.XPATH, '//p[contains(text(),"在贷笔数")]/preceding-sibling::*[1]')
	admin = (By.XPATH, "//div[@class='user-info']")
	opt_log = (By.XPATH, "//div[@class='operation-log-menu']")
	logo = (By.XPATH, "//h1[text()='小贷业务系统']")

	# noinspection PyMethodMayBeStatic
	def menu_element(self, menu_name: str) -> tuple:
		res = (By.XPATH, f"//span[text()='{menu_name}']")
		return res

	def show_menu(self, menu_name: str):
		"""
		显示菜单

		:param menu_name: 业务查询=0,财务统计=1,业务管理=2,业务统计=3

		:type menu_name: str
		"""
		self.find_element(*self.logo)
		if menu_name == "0":
			menu_name = "sub_menu_1_$$_/businessQuery-popup"
		elif menu_name == "1":
			menu_name = "sub_menu_2_$$_/financialStatistics-popup"
		elif menu_name == "2":
			menu_name = "sub_menu_3_$$_/businessManagement-popup"
		elif menu_name == "3":
			menu_name = "sub_menu_4_$$_/businessStatistics-popup"
		else:
			raise KeyError(f"不存在的菜单ID:{menu_name}")
		self.excute_script(f"document.getElementById('{menu_name}').removeAttribute('style')")

	def hidden_menu(self, menu_name: str):
		"""
		隐藏菜单

		:param menu_name: 业务查询=0,财务统计=1,业务管理=2,业务统计=3

		:type menu_name: str
		"""
		self.find_element(*self.logo)
		if menu_name == "0":
			menu_name = "sub_menu_1_$$_/businessQuery-popup"
		elif menu_name == "1":
			menu_name = "sub_menu_2_$$_/financialStatistics-popup"
		elif menu_name == "2":
			menu_name = "sub_menu_3_$$_/businessManagement-popup"
		elif menu_name == "3":
			menu_name = "sub_menu_4_$$_/businessStatistics-popup"
		else:
			raise KeyError(f"不存在的菜单ID:{menu_name}")
		self.excute_script(f"document.getElementById('{menu_name}').setAttribute('style','display:None')")

	@allure.step("检查首页是否正常")
	def check_index(self):
		with soft_assertions():
			amount = float(self.find_element(*self.amount).text.replace(',', ''))
			running_days = int(self.find_element(*self.running_days).text.replace(',', ''))
			user_count = int(self.find_element(*self.user_count).text.replace(',', ''))
			count = int(self.find_element(*self.count).text.replace(',', ''))

			assert_that(amount, "断言在贷总金额").is_greater_than(0.00)
			assert_that(running_days, "断言业务运行天数").is_greater_than(0)
			assert_that(user_count, "断言用户数").is_greater_than(0)
			assert_that(count, "断言在贷笔数").is_greater_than(0)

	@allure.step("跳转业务开关")
	def page_business_switch(self):
		self.show_menu("2")

		def _click():
			self.element_click(*self.menu_element("业务开关"))

		self.loop_(_click)
		return BusinessSwitch(self.driver)

	@allure.step("跳转授信页面")
	def page_credit(self):
		self.show_menu("0")

		def _click():
			self.element_click(*self.menu_element("授信"))

		self.loop_(_click)
		return Credit(self.driver)

	@allure.step("跳转进件")
	def page_apply(self):
		self.show_menu("0")

		def _click():
			self.element_click(*self.menu_element("进件"))

		self.loop_(_click)
		return Apply(self.driver)

	@allure.step("跳转借款统计")
	def page_loan_statistics(self):
		self.show_menu("3")

		def _click():
			self.element_click(*self.menu_element("借款统计"))

		self.loop_(_click)
		return LoanStatistics(self.driver)

	@allure.step("跳转放款统计")
	def page_lend_statistics(self):
		self.show_menu("3")
		self.element_click(*self.menu_element("放款统计"))
		return LendStatistics(self.driver)

	@allure.step("跳转资产列表")
	def page_asset_list(self):
		self.show_menu("0")

		def _click():
			self.element_click(*self.menu_element("资产"))

		self.loop_(_click)
		return AssetList(self.driver)

	@allure.step("跳转操作日志")
	def page_operate_log(self):
		self.move_to_element(self.find_element(*self.admin))
		self.element_click(*self.opt_log)
		return OperateLog(self.driver)

	@allure.step("跳转还款")
	def page_repayment(self):
		self.show_menu("0")
		self.element_click(*self.menu_element("还款"))
		return Repayment(self.driver)

	@allure.step("跳转债转")
	def page_swap(self):
		self.show_menu("0")
		self.element_click(*self.menu_element("债转"))
		return Swap(self.driver)

	@allure.step("跳转用户额度")
	def page_quote_center(self):
		self.show_menu("0")

		def _click():
			self.element_click(*self.menu_element("用户额度"))

		self.loop_(_click)
		return QuoteCenter(self.driver)

	@allure.step("跳转分账报表")
	def page_split_report(self):
		self.show_menu("1")
		self.element_click(*self.menu_element("分账报表"))
		return SplitReport(self.driver)

	@allure.step("跳转债转合同确认")
	def page_swap_contract_confirm(self):
		self.show_menu("1")

		def _click():
			self.element_click(*self.menu_element("债转合同确认"))

		self.loop_(_click)
		return SwapContractConfirm(self.driver)

	@allure.step("跳转分润汇总")
	def page_profit_shareing(self):
		self.show_menu("1")
		self.element_click(*self.menu_element("分润汇总"))
		return ProfitShareing(self.driver)

	@allure.step("跳转分润明细")
	def page_profit_shareing(self):
		self.show_menu("1")
		self.element_click(*self.menu_element("分润明细"))
		return ProfitShareing(self.driver)

	@allure.step("跳转分润2019")
	def page_profit_shareing_2019(self):
		self.show_menu("1")
		self.element_click(*self.menu_element("分润2019"))
		return ProfitShareing2019(self.driver)

	@allure.step("跳转资金流水")
	def page_capital_flow(self):
		self.show_menu("1")
		self.element_click(*self.menu_element("资金流水"))
		return CapitalFlow(self.driver)

	@allure.step("跳转放款确认")
	def page_loan_confirm(self):
		self.show_menu("1")
		self.element_click(*self.menu_element("放款确认"))
		return LoanConfirm(self.driver)

	@allure.step("跳转渠道产品额度")
	def page_channel_product_quote(self):
		self.show_menu("2")

		def _click():
			self.element_click(*self.menu_element("渠道产品额度"))

		self.loop_(_click)
		return ChannelProductQuote(self.driver)
