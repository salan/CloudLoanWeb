# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 13:41:00
@describe: 渠道产品额度
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class ChannelProductQuote(Base):
	adjust_buttons = (By.XPATH, "//a[text()='调整']")
	confirm_button = (By.XPATH, "//span[text()='确认']")
	adjust_success = (By.XPATH, "//span[text()='额度调整成功']")

	def adjust_quote(self):
		self.find_elements(*self.adjust_buttons)[0].click()

		def _click():
			self.element_click(*self.confirm_button)

		self.loop_(_click)
		self.find_element(*self.adjust_success)
