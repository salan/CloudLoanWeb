# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 09:33:00
@describe: 债转合同确认
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class SwapContractConfirm(Base):
	swap_date = (By.XPATH, "//span[@class='ant-calendar-picker-input ant-input']")
	last_month = (By.XPATH, "//a[@class='ant-calendar-prev-month-btn']")
	start_date = (By.XPATH, "//td[@role='gridcell']/div")
	end_date = (By.XPATH, "//td[@class='ant-calendar-cell ant-calendar-in-range-cell']")
	aria_selected = (By.XPATH, "//div[@aria-selected='true']")

	def __init__(self, driver):
		super().__init__(driver)
		self._search()

	def _search(self):
		self.element_click(*self.swap_date)
		self.find_elements(*self.last_month)[0].click()
		self.find_elements(*self.start_date)[0].click()
		try:
			self.find_elements(*self.end_date)[-1].click()
		except:
			self.find_elements(*self.aria_selected)[-1].click()
		self.element_click(*self.saas_elements("search"))

	def swap_contract_confirm_detail(self):
		"""跳转债转确认详情"""

		def _detail():
			self.find_elements(*self.saas_elements("search_detail"))[0].find_elements(By.XPATH, "td")[1].find_element(
				By.XPATH, "a").click()

		self.loop_(_detail)
		return SwapContractConfirmDetail(self.driver)


class SwapContractConfirmDetail(Base):
	count = (By.XPATH, "//div[text()='笔数汇总：']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.count)
