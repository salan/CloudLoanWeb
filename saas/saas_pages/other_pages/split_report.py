# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 09:25:00
@describe: 分账报表
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class SplitReport(Base):
	report_download = (By.XPATH, "//h1[text()='报表下载']")

	@allure.step("检查分账报表是否跳转成功")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.report_download)
