# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2023/2/24 10:45
@describe: 分润统计
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class ProfitStatistics(Base):
	sum = (By.XPATH, "//div[text()='分润金额汇总：']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.sum)
		# 默认所有产品的T-1日分润 断言汇总金额大于0 如果异常则认为昨日分润任务异常
		assert self.get_text(*self.sum) > 0
