# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 13:38:00
@describe: 用户额度
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class QuoteCenter(Base):
	quote_amt = (By.XPATH, "//div[text()='总授信额度：']")

	@allure.step("检查用户额度是否成功跳转")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.quote_amt)
