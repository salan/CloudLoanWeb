# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-24 17:26:00
@describe: 资产列表
"""

import time

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class AssetList(Base):
	loan_amount = (By.XPATH, "//div[text()='放款总金额：']")
	closed_asset = (By.XPATH, "//span[text()='已结清']/preceding-sibling::span")
	cancel_asset = (By.XPATH, "//span[text()='取消']/preceding-sibling::span")
	channel_cloudloan = (By.XPATH, "//span[text()='国投云贷']/preceding-sibling::span")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.loan_amount)

	@allure.step("跳转资产详情")
	def page_asset_detail(self, asset_status: str = None):
		def _click_date_clean():
			self.element_click(*self.saas_elements("date_clean"))

		self.loop_(_click_date_clean)
		if asset_status:
			if asset_status == "已结清":
				self.element_click(*self.closed_asset)
				self.element_click(*self.cancel_asset)
			elif asset_status == "债转":
				self.element_click(*self.loan_amount)
				self.find_elements(*self.saas_elements("label_all"))[1].click()
				self.element_click(*self.channel_cloudloan)
		self.element_click(*self.saas_elements("search"))
		first_detail = self.find_elements(*self.saas_elements("search_detail"))[0]
		asset_id = first_detail.find_elements(By.XPATH, "td")[0].text

		def _click_id():
			first_detail.find_elements(By.XPATH, "td")[0].click()

		self.loop_(_click_id)
		return AssetDetail(self.driver, asset_id)


class AssetDetail(Base):
	"""资产详情"""
	ins_repayment_plan = (By.XPATH, "//span[text()='机构还款计划']/following-sibling::a")
	repay_record = (By.XPATH, "//span[text()='还款记录']/following-sibling::a")
	swap_record = (By.XPATH, "//span[text()='债转记录']/following-sibling::a")

	@allure.step("检查资产详情是否跳转成功")
	def __init__(self, driver, y):
		super().__init__(driver)
		asset_id = (By.XPATH, f"//span[text()='{y}']")
		self.find_element(*asset_id)

	@allure.step("进入机构还款计划列表")
	def page_repayment_plan_list(self):
		self.scroll(element=self.find_element(*self.ins_repayment_plan))
		self.element_click(*self.ins_repayment_plan)
		return RepaymentPlanList(self.driver)

	@allure.step("进入还款记录")
	def page_repay_record(self):
		self.scroll(element=self.find_element(*self.repay_record))
		self.element_click(*self.repay_record)
		return RepayRecord(self.driver)

	@allure.step("进入债转记录")
	def page_swap_record(self):
		self.scroll(element=self.find_element(*self.swap_record))
		self.element_click(*self.swap_record)
		return SwapRecord(self.driver)


class RepayRecord(Base):
	"""还款记录"""

	repay_amt = (By.XPATH, "//div[text()='还款金额汇总']")

	@allure.step("检查还款记录是否跳转成功")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.repay_amt)


class SwapRecord(Base):
	"""债转记录"""
	swap_amt = (By.XPATH, "//div[text()='债转金额汇总']")

	@allure.step("检查债转记录是否跳转成功")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.swap_amt)


class RepaymentPlanList(Base):
	"""机构还款计划"""
	curAmount = (By.XPATH, "//div[text()='应付金额汇总']")
	relief = (By.XPATH, "//span[text()='添加减免申请']")

	@allure.step("检查机构还款计划列表")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.curAmount)

	@allure.step("进入减免申请页面")
	def page_relief(self):
		self.element_click(*self.relief)
		return Relief(self.driver)


class Relief(Base):
	rest_amount = (By.XPATH, "//span[text()='减免后应还金额']")

	@allure.step("检查减免申请")
	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.rest_amount)
