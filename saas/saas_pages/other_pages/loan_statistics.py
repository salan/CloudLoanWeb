# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-24 17:00:00
@describe: 借款统计
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class LoanStatistics(Base):
	total_apply = (By.XPATH, "//div[text()='借款申请总金额']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.total_apply)
