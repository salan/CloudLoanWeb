# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-21 15:26:00
@describe: 授信页面
"""
import time

import allure
from selenium.webdriver.common.by import By
from typing import List
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.support import expected_conditions as EC

from common.base import Base


class Credit(Base):
	title = (By.XPATH, "//div[text()='授信总金额：']")
	search = (By.XPATH, "//span[text()='搜索']")

	product_list = (By.XPATH, "//label[@class='ant-checkbox-wrapper']")

	def __init__(self, driver):
		super().__init__(driver)
		assert self.find_element(*self.title)

	@allure.step("跳转授信详情页面")
	def page_credit_detail(self, ele: List[WebElement], y):
		ele[0].find_elements(by=By.XPATH, value="td")[0].find_element(by=By.XPATH, value="a").click()
		return CreditDetail(self.driver, y)

	@allure.step("检查查询结果是否正确")
	def check_search_result(self, ele: List[WebElement], product_name: str):
		if ele:
			for e in ele:
				name = e.find_elements(by=By.XPATH, value="td")[3].text
				assert name == product_name
		else:
			self.skip_case("无数据，跳过用例")

	@allure.step("检查筛选结果与授信详情")
	def product_list_search(self):
		self.element_click(*self.saas_elements("collapsed_up"))
		for prod in self.find_elements(*self.product_list)[0:4]:
			product_name = prod.find_element(by=By.XPATH, value="span[2]").text
			prod.find_element(by=By.XPATH, value="span[1]").click()
			self.element_click(*self.saas_elements("search"))
			# 获取授信列表元素
			detail_list = self.find_elements(*self.saas_elements("search_detail"))
			# 检查元素内容
			self.check_search_result(detail_list, product_name)
			self.page_credit_detail(detail_list, product_name)
			self.driver.back()
			self.element_click(*self.saas_elements("reset"))


class CreditDetail(Base):
	@allure.step("检查授信详情页面客户信息")
	def __init__(self, driver, y):
		super().__init__(driver)
		company_name = (By.XPATH, "//span[text()='公司名称：']")
		customer_name = (By.XPATH, "//span[text()='姓名：']")
		sys_info = (By.XPATH, "//div[text()='系统信息'] ")
		self.find_element(*sys_info)
		if y == "订车贷":
			res = EC.invisibility_of_element_located(company_name)
			assert not res(self.driver)
		else:
			res = EC.invisibility_of_element_located(customer_name)
			assert not res(self.driver)
