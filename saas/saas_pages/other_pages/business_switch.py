# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: 业务开关页面
"""

import time

import allure
from selenium.webdriver.common.by import By
from common.base import Base


class BusinessSwitch(Base):
	confirm_effect_buttons = (By.XPATH, "//div[@class='ant-form-item-control-input-content']/button")
	effect_success_buttons = (By.XPATH, "//span[text()='生效']")
	roma_button = (By.XPATH, "//div[text()='罗马车贷']")

	@allure.step("跳转业务开关并检查")
	def __init__(self, driver):
		super().__init__(driver)
		header = (By.XPATH, "//div[text()='海尔云贷']")
		assert self.get_text(*header) == "海尔云贷"

	@allure.step("生效业务开关")
	def effect_business_switch(self):
		self.scroll_to_floor()
		self.element_click(*self.roma_button)
		time.sleep(0.5)
		self.scroll_to_floor()
		switch_status_before = self.find_elements(*self.confirm_effect_buttons)[-1].get_attribute("aria-checked")
		self.find_elements(*self.confirm_effect_buttons)[-1].click()
		self.find_elements(*self.effect_success_buttons)[1].click()
		self.find_elements(*self.effect_success_buttons)[2].click()
		switch_status_after = self.find_elements(*self.confirm_effect_buttons)[-1].get_attribute("aria-checked")
		if switch_status_before == "OPEN":
			assert switch_status_after == "CLOSE"
		elif switch_status_before == "CLOSE":
			assert switch_status_after == "OPEN"
		else:
			raise
		self.driver.refresh()
		self.scroll_to_floor()
		self.element_click(*self.roma_button)
		time.sleep(0.5)
		self.scroll_to_floor()
		self.find_elements(*self.confirm_effect_buttons)[-1].click()
		self.find_elements(*self.effect_success_buttons)[1].click()
		self.find_elements(*self.effect_success_buttons)[2].click()
