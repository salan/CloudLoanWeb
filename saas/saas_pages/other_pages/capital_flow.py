# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-08-26 10:08:00
@describe: 资金流水
"""

import allure
from selenium.webdriver.common.by import By

from common.base import Base


class CapitalFlow(Base):
	sum = (By.XPATH, "//div[text()='金额总计：']")

	def __init__(self, driver):
		super().__init__(driver)
		self.find_element(*self.sum)
