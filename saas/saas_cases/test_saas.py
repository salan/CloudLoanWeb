# -*- coding: UTF-8 -*-
"""
@auth:buxiangjie
@date:2020-05-12 11:26:00
@describe: 测试用例
"""
import allure
import pytest

from saas.saas_pages.index import Index


# @allure.feature(f"SAAS系统测试{random.random()}")
class TestSaas:

	@allure.title("saas首页")
	@allure.severity("blocker")
	def test_check_index(self, drivers):
		"""检查saas首页"""
		Index(drivers).check_index()

	@allure.title("操作日志详情")
	@allure.severity("normal")
	def test_check_operate_log_detail(self, drivers):
		"""检查操作日志详情"""
		Index(drivers).page_operate_log().page_operate_log_detail()

	@allure.title("授信列表与授信详情检查")
	def test_check_credit_list_and_detail(self, drivers, back_business_management):
		"""授信列表与授信详情检查"""
		Index(drivers).page_credit().product_list_search()

	@allure.title("进件列表与详情检查")
	@allure.severity("blocker")
	def test_check_apply_list_and_detail(self, drivers, back_business_management):
		"""进件列表与详情检查"""
		Index(drivers).page_apply().check_apply_list_and_detail()

	@allure.title("借款统计")
	@allure.severity("critical")
	def test_check_loan_statistics(self, drivers, back_business_statistics):
		"""检查借款统计"""
		Index(drivers).page_loan_statistics()

	@allure.title("放款统计")
	@allure.severity("critical")
	def test_check_lend_statistics(self, drivers, back_business_statistics):
		"""检查放款统计"""
		Index(drivers).page_lend_statistics()

	@allure.title("资产列表")
	@allure.severity("blocker")
	def test_check_asset_list(self, drivers, back_business_management):
		"""检查资产列表"""
		Index(drivers).page_asset_list()

	@allure.title("资产详情")
	@allure.severity("blocker")
	def test_check_asset_detail(self, drivers, back_business_management):
		"""检查资产详情"""
		Index(drivers).page_asset_list().page_asset_detail()

	@allure.title("机构还款计划")
	@allure.severity("blocker")
	def test_check_repayment_plan(self, drivers, back_business_management):
		"""检查机构还款计划"""
		Index(drivers).page_asset_list().page_asset_detail().page_repayment_plan_list()

	@allure.title("还款记录")
	@allure.severity("blocker")
	def test_check_repay_record(self, drivers, back_business_management):
		"""检查还款记录"""
		Index(drivers).page_asset_list().page_asset_detail("已结清").page_repay_record()

	@allure.title("债转记录")
	@allure.severity("blocker")
	def test_check_swap_record(self, drivers, back_business_management):
		"""检查债转记录"""
		Index(drivers).page_asset_list().page_asset_detail("债转").page_swap_record()

	@allure.title("还款")
	@allure.severity("blocker")
	def test_check_repayment(self, drivers, back_business_inquiry):
		"""检查还款"""
		Index(drivers).page_repayment()

	@allure.title("债转")
	@allure.severity("critical")
	def test_check_swap(self, drivers, back_business_management):
		"""检查债转"""
		Index(drivers).page_swap()

	@allure.title("用户额度")
	@allure.severity("blocker")
	def test_check_quote_center(self, drivers, back_business_management):
		"""检查用户额度"""
		Index(drivers).page_quote_center()

	@allure.title("债转合同确认")
	@allure.severity("blocker")
	def test_check_swap_contract_confirm_detail(self, drivers, back_financial_statistics):
		"""债转合同确认详情"""
		Index(drivers).page_swap_contract_confirm().swap_contract_confirm_detail()

	@allure.title("分润汇总")
	@allure.severity("blocker")
	def test_check_profit_statistics(self, drivers, back_financial_statistics):
		"""检查分润汇总"""
		Index(drivers).page_profit_shareing()

	@allure.title("分润明细")
	@allure.severity("blocker")
	def test_check_profit_shareing(self, drivers, back_financial_statistics):
		"""检查分润明细"""
		Index(drivers).page_profit_shareing()

	@allure.title("分润2019")
	@allure.severity("blocker")
	def test_check_profit_shareing_2019(self, drivers, back_financial_statistics):
		"""检查分润2019"""
		Index(drivers).page_profit_shareing_2019()

	@allure.title("资金流水")
	@allure.severity("critical")
	def test_check_capital_flow(self, drivers, back_financial_statistics):
		"""检查资金流水"""
		Index(drivers).page_capital_flow()

	@allure.title("放款确认")
	@allure.severity("blocker")
	def test_check_loan_confirm(self, drivers, back_financial_statistics):
		"""检查放款确认"""
		Index(drivers).page_loan_confirm()

	@allure.title("调整渠道产品额度")
	@allure.severity("blocker")
	def test_adjustment_channel_quote(self, drivers, back_business_management):
		"""调整渠道产品额度"""
		Index(drivers).page_channel_product_quote().adjust_quote()

	@allure.title("生效业务开关")
	@allure.severity("blocker")
	def test_effect_business_switch(self, drivers, back_business_management):
		"""生效业务开关"""
		Index(drivers).page_business_switch().effect_business_switch()

	@allure.title("减免申请")
	@allure.severity("blocker")
	def test_relief(self, drivers, back_business_inquiry):
		"""检查减免申请"""
		Index(drivers).page_asset_list().page_asset_detail().page_repayment_plan_list().page_relief()


if __name__ == '__main__':
	pytest.main()
