__author__ = 'buxiangjie'
"""
@time: 2024/2/9 22:42
"""

import allure

from common.base import Base
from selenium.webdriver.common.by import By
from zhidemai.zhidemai_pages.member_home import MemberHome
from zhidemai.zhidemai_pages.article_detail import ArticleDetail


class SearchResult(Base):
	member_tab = (By.XPATH, "//a[text()='用户']")
	community_tab = (By.XPATH, "//ul[@class='subcate-tab-list']/li[4]")
	member_name = (By.CLASS_NAME, "search-user-title")
	article_list = (By.XPATH, "//h5[@class='feed-shaiwu-title']/a")

	@allure.step("点击用户tab后跳转个人主页")
	def click_member_tab_to_member_home(self):
		"""
		点击用户tab跳转个人主页
		"""
		self.element_click(*self.member_tab)
		self.element_click(*self.member_name)
		self.driver.switch_to.window(self.driver.window_handles[-1])
		return MemberHome(self.driver)

	@allure.step("点击社区tab后跳转文章详情页")
	def click_community_tab_to_article_detail(self):
		"""
		点击社区tab跳转文章详情页
		"""
		self.element_click(*self.community_tab)
		self.find_elements(*self.article_list)[0].click()
		self.driver.switch_to.window(self.driver.window_handles[-1])
		return ArticleDetail(self.driver)
