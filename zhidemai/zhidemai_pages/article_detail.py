__author__ = 'buxiangjie'
"""
@time: 2024/2/12 21:59
"""

import allure

from common.base import Base
from selenium.webdriver.common.by import By


class ArticleDetail(Base):
	tip = (By.LINK_TEXT, "打赏")

	@allure.step("检查打赏按钮是否存在")
	def check_article_detail_tip_btn(self):
		"""
		检查打赏按钮是否存在
		"""
		assert self.find_element(*self.tip)
