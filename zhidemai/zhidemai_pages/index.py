__author__ = 'buxiangjie'
"""
@time: 2024/2/9 22:28
"""

import allure
from selenium.webdriver.common.by import By
from common.base import Base
from zhidemai.zhidemai_pages.search_result import SearchResult


class Index(Base):
	search_text = (By.ID, "J_search_input")
	search_btn = (By.CLASS_NAME, "search-submit")

	@allure.step("搜索内容")
	def search(self, text: str):
		"""
		首页搜索
		"""
		self.find_element(*self.search_text).send_keys(text)
		self.element_click(*self.search_btn)
		return SearchResult(self.driver)
